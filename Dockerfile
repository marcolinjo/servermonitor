# Use an official Python runtime as a parent image
FROM python:3.11.0a3-slim

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

# Install any needed packages specified in requirements.txt
RUN pip3 install --trusted-host pypi.python.org -r requirements.txt

#testen, ob der richtige Port übergeben wurde
#ENV PORT=443
#ENV DOMAIN=dev.kundenablesung.bs-energy.de
#RUN echo "Versuche, die Variablen zu testen."
#RUN if [ "$PORT" = "" ]; \
#    then echo "Variable Port ist leer."; \
#    else echo "Yeah! Variable Port wurde auf $PORT gesetzt."; \
#    fi

# Make port available to the world outside this container
#EXPOSE ${PORT}

# Run gunicorn
#ENTRYPOINT ["gunicorn"]
#CMD ["-b", "0.0.0.0:2000", "wsgi:create_app()"]
#CMD ["--certfile=./secrets/${DOMAIN}.crt", "--keyfile=./secrets/${DOMAIN}.key", "--bind", "0.0.0.0:2000", "--workers=2", "wsgi:create_app()"]
#CMD gunicorn "wsgi:create_app()" --certfile="./secrets/${DOMAIN}.crt" --keyfile="./secrets/${DOMAIN}.key" --bind "0.0.0.0:2000" --workers=2

CMD python3 main.py ${LOGLEVEL}