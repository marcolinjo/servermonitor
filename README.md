# Servermonitor

Der Servermonitor überwacht die Vitalität eines <SERVICE_ENDPOINT> und reportet dessen Status an einen <INFOWEBHOOK> z.B. für den Telegram-Messenger oder einen Google Chat.

## Wie Du das REPO nutzen kannst?

### Deinen Service überwachen

1. Parametrisiere deine Umgebungsvariablen in .env:<br><br>
`SERVER= Endpunkt, den du überwachen möchtetst z.B. https://www.google.de`<br>
`INFOHOOK= Webhook für Messenger>Webhook für Messenger`<br>
`INTERVALL= Intervall zwischen den Prüfungen z.B. 1`<br>
`MESSAGE= Message, die an den Webhook gesand werden soll z.B. Mayday! Server ist down.`<br><br>
2.Starte den Service mit: `docker-compose up --build --remove-orphans`

### Den Service in deine Anwendung einbauen

1. Kopiere die Inhalte der Daten `./docker-compose.yaml` in Dein Docker-Compose
2. Starte Deine Service Stack mit `docker-compose up --build --remove-orphans`

## Wo du weiter Infos erhältst?

* [Google Chat Webhooks](https://developers.google.com/chat/how-tos/webhooks)
+ [Telegram Webhooks](https://core.telegram.org/bots/api)
* [Discord Webhooks](https://support.discord.com/hc/de/articles/228383668-Einleitung-in-Webhooks)
