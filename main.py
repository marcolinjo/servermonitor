"""
Der Deamon fragt in regelmäßigen Abständen einen Serverendpunkt an.
Sollte der Server nicht erreicht werden, informiert er den Anwender via Webhook.
"""

import sys, os, json
from datetime import datetime
import schedule
import requests
import logging

# endpunkt, der überwacht werden soll
endpunkt_für_überwachung = os.getenv("SERVER")

# pause die zwischen den überwachungen liegen soll in minuten
pause_zwischen_überwachungen_in_minuten = int(os.getenv("INTERVALL"))

# webhook, an die die info gepuscht werdern soll
endpunkt_für_info = os.getenv("INFOHOOK")

# message, die an den webhook gesand werden soll
message_for_user = {
    "text": os.getenv("MESSAGE")
}

# komplexe nachricht
message_for_user_komplex = {
    "cards": [
        {
            "sections": [
                {
                    "widgets": [
                        {
                            "image": {
                                "imageUrl": None
                            }
                        }
                        ,{
                            "buttons": [
                                {
                                    "textButton": {
                                        "text": "Mehr erfahren",
                                        "onClick": {
                                            "openLink": {
                                                "url": None
                                            }
                                        }
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ]
}

#memo für tage, an denen die info schon rausging
days_informed = []

def setup():
    """
    Initialisiert die Variablen des Skripts.
    Wird durch main aufgerufen.
    """
    pass

def run():
    """
    Wird alle x Minuten aufgerufen.
    Ruft Serverprüfung auf.
    Entscheidet über Usernotification.
    :return:
    """
    logging.info("checke serverstatus.")

    server_status = get_server_status()

    if(server_status["code"] != 200):

        logging.info("server sendet: %s", server_status["code"])

        heute = datetime.today().strftime('%Y-%m-%d')

        if(heute not in days_informed):

            notifiy_user(message_for_user)

            notifiy_user(
                    {
                        "text": str(server_status["antwort"])
                    }
            )
            days_informed.append(heute)

        else:

            logging.info("anwender wurde heute schon informiert.")

    else:

        logging.info("server lebt noch.")

def get_server_status():
    """
    Fragt den Server an.
    Gibt dessen Status zurück.
    :return:
    """

    code = None
    antwort = None

    try:

        x = requests.get(endpunkt_für_überwachung, verify=False)

        code = x.status_code
        antwort = str(x.text)

        return {
            "code": code
            , "antwort": antwort
        }

    except:

        logging.error("Server wurde nicht erreicht.")
        logging.error(sys.exc_info())

        return {
            "code": code
            , "antwort": str(sys.exc_info())
        }

def notifiy_user(message_json):
    """
    Informiert den user als Pushnachricht
    :return:
    """

    headers = {
        'Content-Type': 'application/json; charset=UTF-8'
    }

    #print(endpunkt_für_info, message_for_user)

    try:

        r = requests.post(

            url=endpunkt_für_info,
            data=json.dumps(message_json),
            headers=headers
        )

        logging.info("anwender wurde informiert.")

        return {

            "status": True
        }

    except:

        logging.error("anwender wurde nicht informiert.")
        logging.error(sys.exc_info())

        return {

            "status": False
            ,"error": sys.exc_info()
        }


if __name__ == "__main__":

    if("info" in os.getenv("LOGLEVEL")):
        logging.getLogger().setLevel(logging.INFO)

    logging.info("servermonitor wurde gestartet.")
    logging.info("für server: %s", endpunkt_für_überwachung)
    logging.info("mit intervall: %s", pause_zwischen_überwachungen_in_minuten)
    logging.info("auf webhook: %s", endpunkt_für_info)

    schedule.every(pause_zwischen_überwachungen_in_minuten).minutes.do(run)

    while True:
        schedule.run_pending()
